﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Security.Principal;

namespace MultiloansCRM.Models
{
    public class UserAccount
    {
        [Key]
        public int Id { get; set; }

        public bool Confirmed { get; set; }

        [Required(ErrorMessage = "Imię jest wymagane.")]
        public string Firstname { get; set; }

        [Required(ErrorMessage = "Nazwisko jest wymagane.")]
        public string Lastname { get; set; }

        [Required(ErrorMessage = "Adres email jest wymagany.")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required(ErrorMessage = "Nazwa użytkownika jest wymagana.")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Hasło jest wymagane.")]
        public string Password { get; set; }

        [Compare("Password", ErrorMessage = "Powtórz hasło.")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }

        public string Salt { get; set; }

        public string Roles { get; set; }
    }
}