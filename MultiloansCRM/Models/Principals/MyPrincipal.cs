﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace MultiloansCRM.Models.Principals
{
    public class MyPrincipal : IPrincipal
    {
        public MyPrincipal(IIdentity identity)
        {
            Identity = identity;
        }

        public IIdentity Identity
        {
            get;
            private set;
        }

        public UserAccount UserAccount { get; set; }

        public bool IsInRole(string role)
        {
            if (UserAccount.Roles.Split(';').Contains(role))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}