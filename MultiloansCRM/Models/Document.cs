﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MultiloansCRM.Models
{
    public class Document
    {
        [Key]
        public int ID { get; set; }

        public string Surname { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }

        public string Nip { get; set; }
        
        public decimal TwoYearsSales { get; set; }

        public decimal TwoYearsNetProfit { get; set; }

        public int CreditCount { get; set; }

        public decimal MonthlyCreditInstallment { get; set; }

        public int CompanyType { get; set; }
    }
}