﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace MultiloansCRM.Models
{
    public class PGDbContext : DbContext
    {
        public PGDbContext() : base(nameOrConnectionString: "connString") { }
        public DbSet<UserAccount> userAccounts { get; set; }
        public DbSet<Document> document { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("public");

            Database.SetInitializer<PGDbContext>(null);
            base.OnModelCreating(modelBuilder);
        }
    }
}