﻿using MultiloansCRM.Models;
using MultiloansCRM.Models.Principals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MultiloansCRM.Controllers
{
    public class DocumentsController : Controller
    {
        // GET: Documents
        public ActionResult Index()
        {
            using (PGDbContext db = new PGDbContext())
            {
                string userEmail = string.Empty;
                if (HttpContext.User.Identity.IsAuthenticated)
                {
                    userEmail = ((MyPrincipal)(HttpContext.User)).UserAccount.Email;
                }
                return View(db.document.Where(d => d.Email.Equals(userEmail)).ToList()); 
            }
        }
    }
}