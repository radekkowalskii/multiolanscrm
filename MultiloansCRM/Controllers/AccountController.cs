﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;
using System.Web.WebPages.Html;
using MultiloansCRM.Models;
using MultiloansCRM.Models.Principals;
using MailService;
using System.Threading.Tasks;

namespace MultiloansCRM.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        [Authorize(Roles ="Administrator")]
        public ActionResult Index()
        {
            using (PGDbContext db = new PGDbContext())
            {
                return View(db.userAccounts.ToList());
            }
        }

        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Register(UserAccount account)
        {
            if (ModelState.IsValid)
            {
                using (PGDbContext db = new PGDbContext())
                {
                    db.userAccounts.Add(account);
                    db.SaveChanges();
                    
                }
                ModelState.Clear();
                ViewBag.Message = account.Firstname + " " + account.Lastname + " pomyślnie zarejestrowany.";
                MailSender mailSender = new MailSender();
                mailSender.SendEmail(account.Email, "Aktywacja konta w portalu Multiloans CRM", 
                    string.Format("Wybierz poniższy link w celu aktywacji konta <br /> <a href=\"http://localhost:57190/Account/AccountConfirm?t={0}\">Potwierdzenie rejestracji konta</a>", account.Id),
                    true);

            }
            return View();
        }

        [AllowAnonymous]
        public ActionResult Login()
        {

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(UserAccount user, string returnUrl)
        {
            using (PGDbContext db = new PGDbContext())
            {
                var usr = db.userAccounts.Where(u => u.Username == user.Username &&
                                                     u.Password == user.Password &&
                                                     u.Confirmed)
                                                    .FirstOrDefault();
                if (usr != null)
                {
                    usr.Password = string.Empty;
                    usr.ConfirmPassword = string.Empty;
                    usr.Salt = string.Empty;
                    
                    var serializer = new JavaScriptSerializer();
                    string userData = serializer.Serialize(usr);

                    var authTicket = new FormsAuthenticationTicket(
                        1,
                        user.Username,
                        DateTime.Now,
                        DateTime.Now.AddMinutes(20),
                        false,
                        userData);

                    var encryptedTicket = FormsAuthentication.Encrypt(authTicket);

                    var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                    HttpContext.Response.Cookies.Add(authCookie);

                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Nazwa użytkownika lub hasło jest niepoprawne.");
                }
                return View();
            }
        }

        [AllowAnonymous]
        public ActionResult LoggedIn()
        {
            if (User.Identity.IsAuthenticated)
            {
                ViewBag.Message = "Witaj " + ((MyPrincipal)(HttpContext.User)).UserAccount.Firstname;
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        [AllowAnonymous]
        public ActionResult LogOff()
        {
            HttpContext.Session.Abandon();

            FormsAuthentication.SignOut();

            HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, "");
            cookie.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(cookie);

            return RedirectToAction("Login", "Account");
        }

        [AllowAnonymous]
        public async Task<ActionResult> AccountConfirm()
        {
            var accountId = Convert.ToInt32(Request.QueryString["t"]);
            using (PGDbContext db = new PGDbContext())
            {
                var account = db.userAccounts.SingleOrDefault(a => a.Id.Equals(accountId));
                if (account != null)
                {
                    account.Confirmed = true;
                    await db.SaveChangesAsync();
                }
            }

            ViewBag.Message = "Konto zostało pomyślnie aktywowane.";
            return View();
        }
    }
}