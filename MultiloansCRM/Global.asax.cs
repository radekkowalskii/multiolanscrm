﻿using MultiloansCRM.Models;
using MultiloansCRM.Models.Principals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Script.Serialization;
using System.Web.Security;

namespace MultiloansCRM
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        
        }
        

        protected void Application_AuthenticateRequest(Object sender, EventArgs e)
        {
            HttpCookie authCookie = Context.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie == null || string.IsNullOrWhiteSpace(authCookie.Value))
            {
                return;
            }

            FormsAuthenticationTicket authTicket;
            try
            {
                authTicket = FormsAuthentication.Decrypt(authCookie.Value);
            }
            catch (Exception)
            {
                return;
            }


            var identity = new GenericIdentity(authTicket.Name, "Forms");
            var principal = new MyPrincipal(identity);

            //string userData = ((FormsIdentity)(Context.User.Identity)).Ticket.UserData;
            string userData = authTicket.UserData;

            var serializer = new JavaScriptSerializer();
            principal.UserAccount = (UserAccount)serializer.Deserialize(userData, typeof(UserAccount));

            Context.User = principal;

            //string[] roles = authTicket.UserData.Split(';');

            //Context.User = new GenericPrincipal(new GenericIdentity(authTicket.Name), roles);

        }
    }
}
